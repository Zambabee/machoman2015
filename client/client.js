//localhost socket yhteys.
var socket = io.connect();

//openshift socket yhteys.
//var socket = io.connect('http://studenthud-petendomain.rhcloud.com:8000/');

/////////////////////
////Linkkien haku////
/////////////////////

        //Eventhandler haku-napin painamiselle.
        $(document.body).delegate('#searchBtn', 'click', function() {
            var searchWord = $('#searchLinks').val()
            socket.emit('Search', JSON.stringify(searchWord));
        });

        //Eventhandler lisää-napin painamiselle.
        $(document.body).delegate('#addLinkBtn', 'click', function() {
          var link = {};
          if($('#linkURL').val().indexOf("http://") > -1)
          {
            //leikataan http://-alku jos linkki sisältää sen.
            var URL = $('#linkURL').val().split("http://");
            link.URL = URL[1];
          }
          else if($('#linkURL').val().indexOf("https://") > -1)
          {
            //leikataan myös https://-alku pois.
            var URL = $('#linkURL').val().split("https://");
            link.URL = URL[1];
          }
          else
          {
            link.URL = $('#linkURL').val();
          }

          link.name = $('#linkName').val();
          link.desc = $('#linkDesc').val();
          link.category = $('#linkCat').val();

          socket.emit('AddLink', JSON.stringify(link));
        });

        // Tyhjennetään linkkienlisäyssivun tekstikentät.
        socket.on('ClearLinkTextb', function(){
          $('#linkURL').val('');
          $('#linkName').val('');
          $('#linkDesc').val('');
        });

        //Näyttää linkkien haun tulokset HTML-dokumentissa.
        socket.on('SearchResults', function (results) {
          $('#results').empty();
            var links = [];
            var a = "";
            for(var i = 0; i < results.length; i++)
            {
              var URL = fixHTML(results[i].URL);
              var name = fixHTML(results[i].name);
              var description = fixHTML(results[i].description);
              var rating = results[i].rating;
              //Työntää listaan jokaista linkkia varten tarvittavat html elementit.
              links.push( '<div class="col-md-12">'+
                          '<div class="linkWrapper">'+
                          '<a href="http://'+URL+'" target="_blank" style="color: white; font-weight: bold; font-size: 15px;">'+name+'</a>'+
                          '<button class="delBtn btn btn-default pull-right" name="'+URL+'"><span class="glyphicon glyphicon-remove"></span></button>'+
                          '<button class="downBtn btn btn-danger pull-right" name="'+URL+'"><span class="glyphicon glyphicon-thumbs-down"</span></button>'+
                          '<button class="upBtn btn btn-success pull-right" name="'+URL+'"><span class="glyphicon glyphicon-thumbs-up"></span></button>'+
                          '<p class="rating" name="'+URL+'">'+rating+'</p>'+ '</br>'+
                          'Kuvaus:'+
                          '<div class="linkDesc">'+description+'</div>'+
                          '</div>'+
                          '</div>');
            }
            //Työntää listan linkPage.html tiedostoon
            $( "#results" ).append(links);
        });

        //Eventhandler poisto-napin painamiselle.
        $(document.body).delegate('.delBtn', 'click', function() {
            var link = $(this).attr("name");
            var lastSearch = $('#searchLinks').val();
            bootbox.confirm("Haluatko varmasti poistaa linkin?" , function(r){
              if (r == true) {
                socket.emit('DeleteLinks', JSON.stringify(link), lastSearch);
              }
            });
            
        });

/////////////////////
//////IRC CHAT///////
/////////////////////
        var activeChannel = "";
        var connectingToIrc = false;
		    var hasLoggedToIrc = false;
        var clientSideChannels = [];
        var channelCounter = 0;

        // Korvataan sivulle lisättävästä elementistä HTML:n tagimerkit.
        function fixHTML(message){
          var fixed = "";
          for (var i=0; i<message.length; i++){
            if (message[i] == "<")
              fixed += "&lt;";
            else if (message[i] == ">")
              fixed += "&gt;";
            else if (message[i] == "\"")
              fixed += "&quot;";
            else
              fixed += message[i];
          }
          return fixed;
        };

        // Vastaanottaa serveriltä tulevat viestit.
        socket.on('receiveMessage', function(data,type,channels){

				      // Kirjoittaa serveriltä saadun viestin viestikenttään.
		          if (type == "one" && activeChannel == data.to){
		            var a = data.timestamp + " >> "+data.from+": "+data.message+'\n';
		            $( "#chat_messages" ).append(fixHTML(a));
		          }

              // Kirjoittaa serveriltä saadun listan viesteistä tekstikenttään.
		          else if(type == "many"){
		            var a = "";
		            hasLoggedToIrc = true;
		            ircLoginStatus();
		            if(activeChannel == "")
		            	activeChannel = channels[0];
		            for (var i = 0; i<data.length;i++){
		            	if (data[i].to == activeChannel)
		              		a += data[i].timestamp+ " >> "+data[i].from+": "+data[i].message+'\n';
		            }
		            $("#chat_messages").append(fixHTML(a));
		            listChannels(channels);
		          }
		          scrollToBottom();
        });

        // Tulostaa parametrina saamansa viestit viestikenttään.
        function listChannelMessages(messages){
          var a = "";
          $("#chat_messages").empty();
          for(var i=0; i<messages.length;i++){
            if(activeChannel == messages[i].to){
              a += messages[i].timestamp+" >> "+messages[i].from+": "+messages[i].message+'\n';
            }
          }
          $("#chat_messages").append(fixHTML(a));
          scrollToBottom();
        }

        // Tulostaa parametrina saamansa kanavat kanavalistaan.
        function listChannels(channels){
        	clientSideChannels = [];
        	channelCounter = 0;
          var a = "";
          for(var i=0;i<channels.length;i++){
            a += createChannel(channels[i]);
               	var clientChannel = {
    			        id: channelCounter,
    			        name: channels[i]
    		        };
            channelCounter++;
          }

          $( "#channel_list" ).append(a);
          $(".sidebar_channel").css("background-color", "#556063");

          if (activeChannel == "")
            activeChannel = channels[0];
          if (activeChannel != null)
            document.getElementById(matchID(activeChannel)).style.background = "#5c8a97";
        }

        // Eventhandlerit IRC-kirjautumis/uloskirjautumisnappien painallukselle.
        $(document.body).delegate('#irc_login_btn', 'click', function() {
          socket.emit('ircLogin');
          console.log("Yhdistetaan IRC:iin");
          $('#irc_login_btn').attr("disabled", true);
          connectingToIrc = true;
        });
        $(document.body).delegate('#irc_logout_btn', 'click', function() {
          socket.emit('ircLogout');
          console.log("Poistutaan IRC:stä");
          clearIRC();
        });

        // Nollaa IRCciin liittyvät globaalit muuttujat ja tyhjentää käytettävät elementit.
        function clearIRC(){
          activeChannel = "";
          connectingToIrc = false;
    		  hasLoggedToIrc = false;
    		  $('#chat_messages').empty();
    		  $('#channel_list').empty();
    		  $('#user_list').empty();
    		  ircLoginStatus();
    		  clientSideChannels = [];
    		  channelCounter = 0;
        };
        
        // Socket-kuuntelijat eri IRC-tapahtumille.
        socket.on('connectionEstablished', function(){
          hasLoggedToIrc = true;
          connectingToIrc = false;
          $('#irc_login_btn').hide();
          $('#irc_logout_btn').show();
          $("#chat_messages").append("Connected to IRC");
        });
        socket.on('joinChannel',function(data,messages){
          activeChannel = data;
          $("#chat_messages").empty();
          $(".sidebar_channel").css("background-color", "#556063");
          console.log("DATA ON: "+data);
          $( "#channel_list" ).append(createChannel(data));
          channelCounter++;

          document.getElementById(matchID(data)).style.background = "#5c8a97";
          listChannelMessages(messages);
        });
        socket.on('changeChannel',function(messages){
          $(".sidebar_channel").css("background-color", "#556063");
          if(activeChannel != null)
            document.getElementById(matchID(activeChannel)).style.background = "#5c8a97";
          listChannelMessages(messages);
        });
        socket.on('leaveChannel',function(messages,channels){
          $("#channel_list").empty();         
          activeChannel = channels[0];
          listChannels(channels);
          listChannelMessages(messages);   
        });

        // Apufunktio kanavaolion luomiselle.
        function createChannel(data){
    		  var clientChannel = {
      			id: channelCounter,
      			name: data
    		  };
        	clientSideChannels.push(clientChannel);
        	return '<div style="margin-bottom:2px" id="'+channelCounter+'" onclick="changeChannel(this.id)" class="col-md-12 sidebar_channel">'+
          									data+
          									'<button id="'+channelCounter+'" class="btn btn-default btn-xs pull-right" onclick="deleteChannel('+channelCounter+')">X</button>'+
          								'</div>';
          	
        };

        // Yhdistetään saatu kanava-ID kanavan nimeen.
        function matchChannel(id){
        	for (var i=0;i<clientSideChannels.length;i++){
        		if(clientSideChannels[i].id == id){
        			return clientSideChannels[i].name;
        			break;
        		}
        	}
        };

        // Yhdistetään saatu kanavan nimi kanavan ID:hen
        function matchID(name){
        	for (var i=0;i<clientSideChannels.length;i++){
        		if (clientSideChannels[i].name == name){
					    return clientSideChannels[i].id;
					    break;
				    }
        	}
        };

        // Poistutaan kanavalta
        function deleteChannel(dchannel){
        	socket.emit('leaveChannel',matchChannel(dchannel));
        };

        // Päivitetään aktiivinen kanava
        function changeChannel(cchannel){
          activeChannel = matchChannel(cchannel);
          socket.emit('changeChannel',activeChannel);
        };

        // Tulostaa saadun käyttäjälistan käyttäjätauluun valitun kanavan perusteella.
        socket.on('getNames',function(data,channel){
          listUsers(JSON.parse(data),channel);
        });
        function listUsers(data,channel){
          $('#user_list').empty();
          var a = "";
          for (var i=0; i < data.length; i++){
            if (activeChannel == channel){
              a += data[i]+'</br>';
            }
          }
          $('#user_list').append(a);
        }
        
/////////////////////
////REG JA LOGIN/////
/////////////////////

        var user = {};
        var hidden = false;
           
        // luetaan sivulta rekisteröintiin käytettävät tiedot.
        function fetchRegister(){
          user.email = document.getElementById('regEmail').value;
          user.password = CryptoJS.SHA256(document.getElementById('regPass').value).toString();
          user.passwordConfirm = CryptoJS.SHA256(document.getElementById('regPassConfirm').value).toString();
          user.firstname = document.getElementById('firstname').value;
          user.lastname = document.getElementById('lastname').value;
          user.IRC = document.getElementById('IRC_nick').value;
        };

        // luetaan sivulta kirjautumiseen käytettävät tiedot.
        function  fetchLogin(){
          user.LEmail = document.getElementById('logEmail').value;
          user.Lpassword = CryptoJS.SHA256(document.getElementById('logPassword').value).toString();
        };

        // kirjaudutaan sisään.
        function login() {
          fetchLogin();
          var data = JSON.stringify(user);
          console.log(data);
          socket.emit("loginUser",data);
        };

        // kirjaudutaan ulos.
        function logout() {
          socket.emit('logout');
          clearIRC();
        };

        // rekisteröidään käyttäjä.
        function registerUser() {    
          console.log("Rekisteröinti");
          fetchRegister();
          if (user.password != user.passwordConfirm){
            bootbox.alert("Salasanat eivät täsmää.");
          }
          else{
            var data = JSON.stringify(user);
            socket.emit("registerUser",data);           
          }
        };
       
        // muutetaan käyttöliittymän näkymää kirjautumisen tilan mukaan.
	  	  socket.on('hide', function(){
          if (!hidden) {
              hidden = true;
              $('#loginButton').hide();
              $('#regButton').hide();
              $('#logoutButton').show();
          }
          else {
              hidden = false;
              $('#loginButton').show();
              $('#regButton').show();
              $('#logoutButton').hide();
          }
        });

////////////////////////
////////Rating/////////
///////////////////////
		
		// Peukku-ylös napin painallus.
        $(document.body).delegate('.upBtn', 'click', function() {
          var link = {}; 
          link.url = $(this).attr("name");
          link.rating = 1;
          socket.emit("UpdateRating", JSON.stringify(link));
        });

        // Peukku-alas napin painallus.
        $(document.body).delegate('.downBtn', 'click', function() {
          var link = {}; 
          link.url = $(this).attr("name");
          link.rating = -1;
          socket.emit("UpdateRating", JSON.stringify(link));
        });

        // Päivitetään linkin arvosana.
        socket.on('DisplayRating', function(data, URL){
          var rating = JSON.parse(data);
          //Hakee <P> tagin jonka nimi on URL.
          $('p[name="'+URL+'"]').text(rating[0].rating);
        });

////////////////////////
////VIRHEILMOITUKSET////
////////////////////////

        socket.on('Alert', function(data){

        	// Mikäli virheilmoitus tulee kirjautumattoman käyttäjän yrityksestä kirjautua IRCciin,
        	// palautetaan kirjautumisnapin käyttöominaisuus.
        	if (data.split('#')[0] == 'IRCLOGERROR'){
        		bootbox.alert(data.split('#')[1]);
        		$('#irc_login_btn').attr("disabled", false);
        		connectingToIrc = false;
        	}
        	else{
            	bootbox.alert(data);		
        	}

        });

////////////////////////
/////////MUUTA//////////
////////////////////////

        // Mobiiliversion navigointimenun automaattinen piilottaminen sitä klikatessa.
        var smallNavOpen = false;
        function setSmall(){
          smallNavOpen = true;
        }
        $('.navbar-button').on('click', function() {
          if (smallNavOpen){
            $('.navbar-toggle').click();
            smallNavOpen = false;
          }
        });