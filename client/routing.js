angular.module('routing', ['ngRoute']) // ngRoute moduuliin
  // angular.config pakottaa kutsumaan sitä funktiolla, routeProviderilla konfiguroidaan sovelluksessa tarvittavat reititykset, se asetetaan parametriksi funktiolle
  .config( ['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/linkPage', {
        templateUrl: 'linkPage.html'
      })
      .when('/addLink', {
        templateUrl: 'addLink.html'
      })
      .when('/IRC', {
        templateUrl: 'IRC.html'
      })
      .when('/activeUsers', {
        templateUrl: 'activeUsers.html'
      })
      .otherwise( {
        templateUrl: 'frontpage.html'
      });
  }]);