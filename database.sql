DROP DATABASE IF EXISTS studenthud;
CREATE DATABASE studenthud;
USE studenthud;

CREATE TABLE user (
    user_ID INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(60) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(60) NOT NULL,
    IRC_nick VARCHAR(40) NOT NULL
)ENGINE=InnoDB;

CREATE TABLE messages (
    message_ID INT PRIMARY KEY AUTO_INCREMENT,
    message VARCHAR(255),
    user VARCHAR(255),
    channel VARCHAR(255),
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)ENGINE=InnoDB;

CREATE TABLE links (
    URL VARCHAR(255) PRIMARY KEY,
    user INT,
    name VARCHAR(40) NOT NULL,
    description VARCHAR(255) NOT NULL,
    rating INT,
    category VARCHAR(100) NOT NULL,
    FOREIGN KEY (user) REFERENCES user(user_ID)
)ENGINE=InnoDB;

CREATE TABLE linkRating (
    rating_ID INT PRIMARY KEY AUTO_INCREMENT,
    URL VARCHAR(255),
    user INT,
    rating INT,
    FOREIGN KEY (user) REFERENCES user(user_ID),
    FOREIGN KEY (URL) REFERENCES links(URL) ON DELETE CASCADE
)ENGINE=InnoDB;



POISTETTUJA
-------------------------------------------------------------
CREATE TABLE IRC_channel (
    channel_ID INT PRIMARY KEY AUTO_INCREMENT,
    channel_name VARCHAR(60),
    server_name VARCHAR(60)
)ENGINE=InnoDB;

CREATE TABLE user_channels (
    user_ID INT,
    channel_ID INT,
    PRIMARY KEY (user_ID, channel_ID),
    FOREIGN KEY (user_ID) REFERENCES user(user_ID),
    FOREIGN KEY (channel_ID) REFERENCES IRC_channel(channel_ID)
)ENGINE=InnoDB;