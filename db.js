var Database = function() {
    var database = this;
    var superUser = "admin@admin";
    // Luodaan tietokantayhteys.
    this.init = function() {
        if(undefined === this.connection) {
            console.log("init database connection");
            var mysql      = require('mysql');
            this.connection = mysql.createConnection({
                host     : process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost',
                port     : process.env.OPENSHIFT_MYSQL_DB_PORT || '3306',
                user     : process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'root',
                password : process.env.OPENSHIFT_MYSQL_DB_PASSWORD || 'password',
                database : 'studenthud',
                multipleStatements : true
            });
            this.connection.connect();
        };
    };

    function disconnect() {
        this.connection.end();
    };

    // Kirjoitetaan (palaute) tietokantaan.
    this.write = function(channel,user,message) {
        this.connection.query('INSERT INTO messages(channel, user, message) VALUES (?, ?, ?)', [channel,user,message], 
            function(err, result) {
                if (err) throw err;
        });   
    };

    // Ominaisuus viestien (palautteen) lukemiseen tietokannasta (ei käytössä tällä hetkellä)
    this.read = function(user){
        this.connection.query("SELECT * FROM messages WHERE user=?",[user], 
            function(err, result) {
                console.log("result:", result);
        });   
    };

    //Etsii linkkejä tietokannasta linkin nimen, URLin tai kategorian perusteella.
    this.search = function(searchWord, socket){
        var query = "SELECT * FROM links WHERE name LIKE ? OR URL LIKE ? OR category LIKE ? ORDER BY rating DESC";
        this.connection.query(query,["%"+searchWord+"%","%"+searchWord+"%","%"+searchWord+"%"], 
        function(err, result) {
            if (err) throw err;
            console.log("result:", result);
            //Lähettää socketin avulla tulokset clientille (client.js).
            socket.emit('SearchResults', result);
        });   
    };

    //Lisää linkin tietokantaan.
    this.AddLinks = function(data,socket) {
        var query = "INSERT INTO links(URL, user, name, description, rating, category) VALUES (?, ?, ?, ?, ?, ?)";
        var rating = 0;
        //tarkistetaan onko pakolliset kentät täytetty.
        if (data.URL == "" || data.name == "" || data.desc == ""){
            socket.emit('Alert',"Pakolliset kentät ovat tyhjiä!");
        }
        else{
            this.connection.query(query, [data.URL,socket.userID,data.name,data.desc,rating,data.category], function(err) {
                if (err) throw err;
                
                socket.emit('Alert', "Linkki lisätty!");
                //Onnistuneen lisäyksen jälkeen tyhjennetään tekstikentät käyttöliittymästä.
                socket.emit('ClearLinkTextb');
            });             
        }
    };

    //Tarkistaa löytyykö lisättävä linkki jo tietokannasta.
    this.CheckLink = function(data, socket){
        var query = "SELECT URL FROM links WHERE URL=(?)"

        this.connection.query(query, [data.URL], function(err,result) {
            if (err) throw err;
            
            if(result.length == 0){
                //Jos ei niin lisätään linkki.
                database.AddLinks(data,socket);
            }
            else{
                socket.emit('Alert', "Linkki on jo olemassa!");
            }
        });    
    }

    //Poistaa linkin tietokannasta.
    this.DeleteLinks = function (data, socket,lastSearch){
        var query = "DELETE FROM links WHERE URL=(?) AND user=(?)";
        this.connection.query(query,[data, socket.userID], function(err){
            if (err) throw err;
        });
        //päivitetään hakutulokset viimisimmän hakusanan perusteella.
        database.search(lastSearch,socket);
    };

    //Tarkastaa onko käyttäjä lisännyt kyseisen linkin.
    this.CheckLinkDelete = function(data,socket,lastSearch) {
    	if (socket.email == superUser){
    		var query = "DELETE FROM links WHERE URL=(?)";
    		this.connection.query(query,[data],function(err){
    			if (err) throw err;
    		});
    		database.search(lastSearch,socket);
    	}
    	else{
	        var query = "SELECT * FROM links WHERE URL=(?) AND user=(?)";
	        this.connection.query(query, [data,socket.userID], function(err, result) {
	                if (err) throw err;

	                if (result.length == 0){
	                    //Jos ei ole, niin ilmoitetaan siittä.
	                    socket.emit('Alert', "Et voi poistaa muiden lisäämiä linkkejä!")
	                }
	                else{
	                    database.DeleteLinks(data,socket,lastSearch);
	                }
	        });      		
    	}
    };

    // Listataan käyttäjän socket kirjautuneeksi, mikäli tiedot ovat oikeat.
    this.login = function(userData,socket,clients){
        var parsedData = JSON.parse(userData);
        var logEmail = parsedData["LEmail"];
        var logPassw = parsedData["Lpassword"];

        var userQuery = "SELECT * FROM user WHERE email = (?)";
        this.connection.query( userQuery,[ logEmail ], function(err, rows, fields){
            try{

                // Tarkastetaan onko käyttäjän antama salasana oikea.
                if ( rows[0].password == logPassw){
                    console.log("Onnistunut login "+rows[0].email);
                    socket.name = rows[0].firstname+" "+rows[0].lastname;
                    socket.email = rows[0].email;
                    socket.userID = rows[0].user_ID;
                    socket.IRCnick = rows[0].IRC_nick;
                    console.log("SOCKETIN NIMI: "+socket.email);
                    for(var i = 0; i<clients.length;i++){
                    	if(socket.id == clients[i].id){
                    		clients[i].name = socket.name;
                    		clients[i].email = socket.email;
                    	}
                    }
                    //emitti nappienpiilotusfunktiolle
                    socket.emit('hide');
                }
                else{
                    console.log("ei tunnistetta tietokannassa.");
                }
            }
            catch(err){
                console.log("ei tietokantavertailua.");
            }
        }); 
    };

    // Uuden käyttäjän rekisteröinti
    this.register = function(userData,socket){
        var parsedData = JSON.parse(userData);
        var regEmail = parsedData["email"];
        var firstname = parsedData["firstname"];
        var lastname = parsedData["lastname"];
        var IRC_nick = parsedData["IRC"];
        var hashpsw = parsedData["password"];

        var checkQuery = "SELECT email FROM user WHERE email = (?)";
        
        if (regEmail == "" || firstname == "" || lastname == "" || IRC_nick == "" || hashpsw == ""){
            socket.emit('Alert',"Pakolliset kentät ovat tyhjiä!");
        }

        else{
            // Tarkistetaan, onko sähköpostiosoite jo rekisteröity.
            this.connection.query(checkQuery, [regEmail], function(err, result){
                if (err) throw err;

                // Rekisteröidään käyttäjä.
                if(result.length == 0){
                    database.addData(userData);    
                }
                else{
                    // Rekisteröidystä sähköpostista ilmoitus.
                    console.log("sähköposti löytyy" + result);
                    socket.emit('Alert', "Sähköposti on jo rekisteröity!");
                }
            });       
        }
    };
    // Apufunktio käyttäjän lisäämiseksi tietokantaan.
    this.addData = function(userData,socket){

        var parsedData = JSON.parse(userData);
        var regEmail = parsedData["email"];
        var firstname = parsedData["firstname"];
        var lastname = parsedData["lastname"];
        var IRC_nick = parsedData["IRC"];
        var hashpsw = parsedData["password"];

        var addData = "INSERT INTO user (email, firstname, lastname, password, IRC_nick) VALUES (?, ?, ?, ?, ?)";
        this.connection.query(addData, [ regEmail, firstname, lastname, hashpsw, IRC_nick  ]);
    }

    // Päivittää (ja poistaa linkin mikäli linkin rating on pienempi kuin -10) linkin arvosanan tietokantaan.
    this.UpdateRating = function(data,socket){
    var query = "UPDATE links SET rating=(rating+?) WHERE URL=?; DELETE FROM links WHERE rating<(-10)";
    var URL = data.url;
    this.connection.query(query,[data.rating, URL], function(err){
        if(err) throw err;
        database.ShowRating(URL,socket);
    });
  };

  // Päivittää linkin kokonaisarvosanan käyttöliittymään.
  this.ShowRating = function(URL,socket){
    var query = "SELECT rating FROM links WHERE URL=?";
    this.connection.query(query,[URL], function(err, result){
        if(err) throw err;
        socket.emit('DisplayRating',JSON.stringify(result),URL);
    });
  };

  // Tarkastaa onko käyttäjä arvostellut jo kyseistä linkkiä.
  this.CheckRating = function(data,socket){
    var query = "SELECT * FROM linkRating WHERE user=? AND URL=?;";
    var ratingStatus;

    this.connection.query(query,[socket.userID, data.url], function(err, result){
        if(result.length == 0){//jos käyttäjä ei ole arvostellut linkkiä.
            ratingStatus = 0;   
        }
        else if(result[0].rating == data.rating){//jos käyttäjä on arvostellut linkkiä ja rating on sama.
            ratingStatus = 1;
        }
        else{//jos käyttäjä vaihtaa arvostelunsa.
            ratingStatus = 2;
        }
        //päivitetään tietokannan ratingList-tauluun käyttäjän valinta kyseisen linkin ostalta.
        database.UpdateRatingList(ratingStatus, data, socket);  
    });
  };

  //Tallentaa tietokantaan mitä linkkiä käyttäjä on arvostellut, jotta samaa linkkiä ei voi arvostella uudestaan.
  this.UpdateRatingList = function(ratingStatus,data, socket){
    var query;

    if (ratingStatus == 2){
        query = "UPDATE linkRating SET rating=(?) WHERE URL=? AND user=?";
    }
    else if(ratingStatus == 0){
        query = "INSERT INTO linkRating(rating,URL,user) VALUES (?,?,?)";
    }

    this.connection.query(query,[data.rating,data.url,socket.userID], function(err, result){
        //Päivitetään myös tietokannan links-tauluun linkin kokonais rating.
        if(ratingStatus == 0){
            database.UpdateRating(data, socket);
        }
        else if(ratingStatus == 2){
            data.rating *= 2;
            database.UpdateRating(data, socket);
        }   
    });
  };

};

module.exports = Database;