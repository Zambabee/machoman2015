var Client = function(socket,server) {

	// Määritellään IRC-yhteydelle parametrit.
	var config = {
		server: server,
		userID: socket.userID,
		clientName: socket.IRCnick,
		socket: socket,
		socketID: socket.id
	};
	var messages = [];
	var channelList = [];
	var loggedIn = false;
	var lastAction = new Date();

	// Luodaan IRC-yhteys
	var irc = require('irc');
	var user = new irc.Client(config.server, config.clientName);

	// Katkaistaan IRC-yhteys
	this.disconnect = function(){
		updateLastAction();
		user.disconnect("", function() {
    	});
	};

	this.sendMessage = function(channel,data){
		updateLastAction();
		if (channel != null)
			user.say(channel, data);
	};
	this.joinChannel = function(mchannel,socket){
		updateLastAction();
		if (loggedIn){
			var ok = true;

			// Tarkistetaan löytyykö kanava / käyttäjä jo kanavalistasta.
			for (var i=0; i<channelList.length; i++){
				if (channelList[i] == mchannel){
					// Mikäli löytyy, ohitetaan kaikki jatkotoimenpiteet vaihtamalla boolean ok epätodeksi.
					ok = false;
					break;
				}
			}

			// Liitytään kanavalle.
			if (mchannel[0] == '#' && ok){
				user.join(mchannel,function(){
					socket.emit('joinChannel',mchannel,messages);
					channelList.push(mchannel);
				});
			}

			// Tai lisätään käyttäjä kanavalistaan.
			else if (ok){
				user.whois(mchannel, function(nick){
					if (nick.host && nick.nick != config.clientName){
						socket.emit('joinChannel',mchannel,messages);
						channelList.push(mchannel);
					}
					else{
						console.log("No such user.")
					}
				});
			}
			else{
				console.log("Duplicate channel.")
			}		
		}
	};

	// Lähdetään pois kanavalta.
	this.leaveChannel = function(lchannel,socket){
		updateLastAction();
		for (var i = 0;i<channelList.length;i++){
			if(channelList[i] == lchannel){
				try{
					user.part(lchannel, function(){
						channelUsers(socket);
					});
				}
				catch(err){
					console.log(err);
				}
				// Poistetaan kanava kanavalistasta.
				channelList.splice(i,1);
				channelUsers(socket);
				console.log(channelList);
				break;
			}
		}
	};

	// Vaihdetaan kanavaa.
	this.changeChannel = function(socket,channel){
		socket.emit('changeChannel',messages);
		if(channel[0] == '#')
			user.send('names',channel);
		else
			socket.emit('getNames',JSON.stringify(""),"");
	};

	// Kanavan käyttäjälistaa koskevat toiminnot.
	function channelUsers(socket){
		socket.emit('leaveChannel',messages,channelList);
		if(channelList.length > 0){
			user.send('names',channelList[0]);	
		}
		else{
			socket.emit('getNames',JSON.stringify(""),"");
		}
	};

	// Päivitetään viimeksi tehdyn toiminnon ajankohta.
	function updateLastAction(){
		lastAction = new Date();
	};

	// IRC-KUUNTELIJAT ERI TAPAHTUMIEN VARALLE.

	// kuuntelee milloin käyttäjä saa yhteyden palvelimeen.
	user.addListener('registered', function(message) {
		socket.emit('connectionEstablished');
		loggedIn = true;
	});

	// kuuntelee virheitä (estää osaltaan IRC-olion kaatumisen).
	user.addListener('error', function(message) {
	    console.log('error: ', message);
	});

	// kuunnellaan kanavalle tulevia käyttäjiä.
	user.addListener('join', function (channel, nick, message) {
		user.send('names',channel);

	});

	// kuunnellaan kanavalta lähteviä käyttäjiä.
	user.addListener('part', function (channel, nick, reason, message) {
		user.send('names',channel);
	});

	// kuunnellaan serverin palauttamaa käyttäjälistaa.
	user.addListener('names', function(channel,nicks) {
		var data = Object.keys(nicks);

		for (var i = 0; i<data.length;i++){
			console.log(data[i]);
		}
		config.socket.emit('getNames',JSON.stringify(data),channel);
	});

	// kuunnellaan viestejä.
	user.addListener('message', function (from, to, message) {

		var currentdate = new Date(); 
		var datetime = currentdate.getHours() + ":"  
	                 + currentdate.getMinutes() + ":" 
	                 + currentdate.getSeconds();

		var data = {
			message: message,
			from: from,
			to: to,
			timestamp: datetime 
		};
		messages.push(data);
		config.socket.emit('receiveMessage',data,"one",channelList);
	});

	// kuunnellaan käyttäjän itsensä lähettämiä viestejä.
	user.addListener('selfMessage', function(to,text) {
			var currentdate = new Date(); 
			var datetime = currentdate.getHours() + ":"  
		                 + currentdate.getMinutes() + ":" 
		                 + currentdate.getSeconds();

			var data = {
				message: text,
				from: config.clientName,
				to: to,
				timestamp: datetime
			};

			messages.push(data);
			config.socket.emit('receiveMessage',data,"one",channelList);
	});

	// kuunnellaan yksityisviestejä.
	user.addListener('pm', function (from, message){

		var currentdate = new Date(); 
		var datetime = currentdate.getHours() + ":"  
	                 + currentdate.getMinutes() + ":" 
	                 + currentdate.getSeconds();

		var data = {
			message: message,
			from: from,
			to: from,
			timestamp: datetime 
		};
		messages.push(data);
		if (checkForUser(from)){
			channelList.push(from);
			config.socket.emit('joinChannel',from);
		}
		config.socket.emit('receiveMessage',data,"one",channelList);
	});

	// tarkastetaan löytyykö käyttäjän nimi kanavalistasta.
	// yksityisviestien tapauksessa, käyttäjiä käsitellään kanavina.
	function checkForUser(ircUser){
		var notFound = true;
		for (var i=0;i<channelList.length;i++){
			if (channelList[i] == ircUser){
				notFound = false;
				break;
			}
		}
		return notFound;
	}

	// Palautusfunktiot olion eri arvoille.
	this.userID = function(){
		return config.userID;
	};
	this.SocketID = function(){
		return config.socketID;
	};
	this.ircName = function(){
		return config.clientName;
	};
	this.lastAction = function(){
		return lastAction;
	};

	// Yhdistetään käyttäjän uusi socket jo olemassaolevaan IRC-olioon.
	this.setSocket = function(data){
		updateLastAction();
		config.socket = data;
		config.socketID = data.id;
		data.emit('receiveMessage',messages,"many",channelList);
		if (channelList.length != 0){
			user.send('names',channelList[0]);			
		}
	};

	// Palautetaan käyttäjän IRC-sessio (lähetetään käyttäjälle viesti / kanava / käyttäjätiedot)
	this.returnSession = function(activeChannel){
		updateLastAction();
		config.socket.emit('receiveMessage',messages,"many",channelList);
		if (channelList.length != 0){
			user.send('names',activeChannel);			
		}
	};
};

module.exports = Client;