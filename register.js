var Register = function(db) {

	// Kutsutaan kirjautumisfunktiota tietokannasta.
	this.loginUser = function(userData,socket,clients){
		db.login(userData,socket,clients);
	};

	// Kutsutaan rekisteröitymisfunktiota tietokannasta.
	this.registerUser = function(userData,socket){
		db.register(userData,socket);
	};
};

module.exports = Register;