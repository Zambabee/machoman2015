// Otetaan käyttöön tietokanta.
var database = require('./db');
var db = new database();
db.init();

// Otetaan käyttöön register-luokka.
var register = require('./register');
var rg = new register(db);

// IRC-Client.
var irc_server = "irc.freenode.com";
var irc_client = require('./irc_client');
var ircClientList = [];
function clientIRC(socket){
	ircClientList.push(new irc_client(socket,irc_server));
}

// http://expressjs.com.
var express = require('express');
var app = require('express')();
var server = require('http').Server(app);

// Asetetaan käyttäjäkansio.
app.use(express.static(__dirname + '/client'));

// Otetaan socket.io käyttöön.
var io = require('socket.io')(server);
var http_clients = [];

// Avataan socket käyttäjän ja serverin keskustelua varten.
io.on('connection', function (socket) {

	// Järjestelmän palautelokero.
	socket.on('feedback', function(data){
		db.write("feedback","a",data);
	});

	// Lisätään käyttäjä aktiivisiin käyttäjiin.
	var user = {
		id: socket.id,
		name: undefined,
		email: undefined
	};
	http_clients.push(user);

	// Poistetaan käyttäjä aktiivisista käyttäjistä kun yhteys suljetaan.
	socket.on('disconnect', function (){
		for(var i=0;i<http_clients.length;i++){

			if (http_clients[i].id === socket.id){
				http_clients.splice(i,1);
			}
		}
	});

	// Rekisteröityneen käyttäjän poisto aktiivisista käyttäjistä.
	socket.on('logout', function(){
		delete socket.name;
		delete socket.email;
		delete socket.userID;
		delete socket.IRCnick;
			for(var i=0;i<http_clients.length;i++){
				if (http_clients[i].id == socket.id){
				delete http_clients[i].name;
				delete http_clients[i].email;
				break;
				}
			}
		console.log("käyttäjät: " + http_clients);
		socket.emit('hide');
	});

	// Välitetään aktiivisten käyttäjien lista käyttäjälle.
	socket.on('sendActiveUsers',function(){
		socket.emit('sendActiveUsers',http_clients);
  	});

	// Luodaan käyttäjälle IRC-olio keskustelua varten, tai yhdistetään jo olemassaolevaan luokkaan.
	socket.on('ircLogin', function(activeChannel){

		// Tarkastetaan onko käyttäjä kirjautunut sisään.
		if(socket.userID != undefined){
			var a = true;
			for(var i=0; i<ircClientList.length;i++){

				// Tarkastetaan löytyykö käyttäjältä jo käytössä oleva IRC-olio.
				if (ircClientList[i].userID() === socket.userID){

					if (ircClientList[i].SocketID() != socket.id){
						console.log(socket.IRCnick + " rejoined to IRC");
						// Yhdistetään käyttäjä takaisin omaan IRC-olioonsa.
						ircClientList[i].setSocket(socket);
					}
					else{
						ircClientList[i].returnSession(activeChannel);
					}
				a = false;
				break;
				}
			}
			// Luodaan käyttäjällä IRC-olio mikäli sitä ei löydy.
			if (a) clientIRC(socket);
		}
		else{
			socket.emit('Alert', "IRCLOGERROR#Sinun täytyy kirjautua sisään!");
		}
	});

	// Kirjataan käyttäjä ulos IRC:stä.
	socket.on('ircLogout', function(){
		for(var i=0; i<ircClientList.length;i++){
			if (ircClientList[i].userID() === socket.userID){
				deleteClient(i);
				break;
			}
		}	
	});
	function deleteClient(i){
		console.log(ircClientList[i].ircName()+ " disconnected from IRC");
		ircClientList[i].disconnect();
		delete ircClientList[i];
		ircClientList.splice(i,1);
	}

	// IRC-Keskusteluominaisuudet

	socket.on('sendMessage', function(channel,data){
		var check = checkClient();
		if (check != -1)
			ircClientList[check].sendMessage(channel,data);
	});

	socket.on('joinChannel', function(mchannel){
		var check = checkClient();
		if (check != -1)
			ircClientList[check].joinChannel(mchannel,socket);
	});

	socket.on('leaveChannel', function(lchannel){
		var check = checkClient();
		if (check != -1)
			ircClientList[check].leaveChannel(lchannel,socket);	
	});
	
	socket.on('changeChannel',function(channel){
		var check = checkClient();
		if (check != -1)
			ircClientList[check].changeChannel(socket,channel);
	});

	// Katsotaan mikä IRC-olio kuuluu käyttäjälle.
	function checkClient(){
		var foundNothing = true;
		for (var i = 0;i<ircClientList.length;i++){
			if(ircClientList[i].SocketID() == socket.id){
				return i;
				foundNothing = false;
				break;
			}
		}
		if (foundNothing)
			return -1;
	}

	// Tarkastetaan onko IRC-olio ollut käyttämättömänä 24 tunnin ajan ja poistetaan se mikäli näin on.
    setInterval (checkIdleClients, 600000);
	function checkIdleClients(){

		var currentDate = new Date();
		for (var i = 0;i<ircClientList.length;i++){
			console.log(currentDate - ircClientList[i].lastAction());
			if ((currentDate - ircClientList[i].lastAction()) > 864000000){
				deleteClient(i);
			}
		}
	};

	//Linkkien haku / poisto / lisäysominaisuudet.
	socket.on('Search', function (searchWord) {
		db.search(JSON.parse(searchWord), socket);
	});

	socket.on('AddLink', function(data){
		if (socket.userID != undefined){
			db.CheckLink(JSON.parse(data),socket);
		}
		else{
			socket.emit('Alert', "Sinun täytyy kirjautua sisään!");
		}
	});

	socket.on('DeleteLinks', function(data,lastSearch){
		if(socket.userID != undefined){
			db.CheckLinkDelete(JSON.parse(data),socket,lastSearch);
		}
		else{
			socket.emit('Alert', "Sinun täytyy kirjautua sisään!");
		}
	});

	//Linkkien arvostelu.
	socket.on('UpdateRating', function (data) {
		if(socket.userID != undefined){
			db.CheckRating(JSON.parse(data), socket);
		}
		else{
			socket.emit('Alert', "Sinun täytyy kirjautua sisään!");
		}
	});

	//Uuden käyttäjän rekisteröinti
	socket.on("registerUser", function(data){
		rg.registerUser(data,socket);
	});

	//Sisäänkirjautuminen.
	socket.on("loginUser", function(data){
		var checkDouble = false;

		for (var i=0;i<http_clients.length;i++){
			if(http_clients[i].email == JSON.parse(data)["LEmail"]){
				checkDouble = true;
			}
		}
		if (socket.userID != undefined || checkDouble){
			socket.emit('Alert',"Olet jo kirjautunut sisään!");
		}
		else{
			rg.loginUser(data,socket,http_clients);
		}

	});
});

var host = process.env.OPENSHIFT_NODEJS_IP || "localhost";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
// Kuunnellaan porttia
server.listen(port,host, function () {
	console.log('Listening at '+host+":"+ port);
});

// Käytöstä tällä hetkellä poistettu debug-ominaisuus, joka pitäisi serverin käynnissä vakavan virheenkin jälkeen.
/*process.on('uncaughtException', function (err) {
    console.log(err);
});*/ 